﻿$ErrorActionPreference = 'Stop'
$url = 'https://www.tukui.org/client/windows/tukui-windows-3410.zip' # download url, HTTPS preferred
$checksum = '112a23ef11dccec60c077504c303059e'
$checksumtype = 'md5'

$toolsDir = $(Split-Path -parent $MyInvocation.MyCommand.Definition)
Install-ChocolateyZipPackage 'tukui' $url $toolsDir -checksum $checksum -checksumType $checksumType

$tukuiFileFullPath = get-childitem $toolsDir -recurse -include *.msi | Select-Object -First 1

$packageArgs = @{
  packageName = $env:ChocolateyPackageName
  fileType    = 'msi'
  file        = $tukuiFileFullPath
  silentArgs  = '/passive'
}

Install-ChocolateyPackage @packageArgs
